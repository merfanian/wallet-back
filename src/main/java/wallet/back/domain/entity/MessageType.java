package wallet.back.domain.entity;

public enum MessageType {
    TEXT, REVIEW
}
