package wallet.back.domain.dto;

import lombok.Builder;

@Builder
public class MessageDto {
    String message;
}
