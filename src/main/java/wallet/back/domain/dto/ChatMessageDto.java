package wallet.back.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import wallet.back.domain.entity.Message;
import wallet.back.domain.entity.MessageType;

import java.util.Date;
import java.util.UUID;

@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class ChatMessageDto implements IInputDto<Message>, IOutputDto<Message> {

    String id;
    String gathering;
    String message;
    String from;
    String email;
    String avatar;
    MessageType type;
    long date;

    @Override
    public boolean isValid() {
        return false;
    }

    @Override
    public Message fromDto() {
        return Message.builder().id(UUID.randomUUID()).message(this.message).type(this.type).date(new Date().getTime()).build();
    }

    @Override
    public Message updateDto(Message message) {
        return null;
    }

    @Override
    public IOutputDto<Message> toDto(Message message) {
        return ChatMessageDto.builder()
                .from(message.getProfile().getFirstName() + " " + message.getProfile().getLastName())
                .message(message.getMessage())
                .date(message.getDate())
                .id(message.getId().toString())
                .type(message.getType())
                .email(message.getProfile().getUser().getEmail())
                .gathering(message.getGathering().getId().toString())
                .avatar(message.getProfile().getAvatar())
                .build();
    }
}
