package wallet.back.domain.dto;

import lombok.Data;
import wallet.back.domain.entity.User;

@Data
public class InviteUserToGatheringDto implements IInputDto<User> {

    String identifier;
    String email;
    String username;

    boolean isUsername;

    @Override
    public boolean isValid() {
        return !identifier.isEmpty() && identifier.contains("@");
    }

    @Override
    public User fromDto() {
        return null;
    }

    @Override
    public User updateDto(User user) {
        return null;
    }

    public boolean isUsername() {
        return this.isUsername;
    }

    public void extractIdentifier() {
        this.isUsername = identifier.startsWith("@");
        this.email = !isUsername ? identifier : "";
        this.username = isUsername ? identifier.replace("@", "") : "";
    }
}
