package wallet.back.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import wallet.back.domain.entity.Profile;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface ProfileRepository extends CrudRepository<Profile, String> {
    Optional<Profile> findByUserId(UUID userId);

    Optional<Profile> findById(UUID id);
}
