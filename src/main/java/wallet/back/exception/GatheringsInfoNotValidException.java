package wallet.back.exception;

public class GatheringsInfoNotValidException extends Exception {
    public GatheringsInfoNotValidException() {
        super("Gathering's info not valid");
    }
}
