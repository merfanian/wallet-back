package wallet.back.exception;

public class EmailExistsException extends Exception {
    public EmailExistsException() {
        super("user with this email exists");
    }
}
