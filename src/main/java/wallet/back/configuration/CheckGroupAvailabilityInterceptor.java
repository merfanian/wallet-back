package wallet.back.configuration;

import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptor;
import org.springframework.stereotype.Component;
import wallet.back.controller.AuthenticationController;
import wallet.back.controller.UserController;
import wallet.back.domain.entity.User;
import wallet.back.exception.EmailNotExistsException;

@Component
public class CheckGroupAvailabilityInterceptor implements ChannelInterceptor {

    public static AuthenticationController authenticationController;
    public static UserController userController;

    private void handleToken(StompHeaderAccessor wrap, StompPrincipal user) {
        String usernameFromToken;
        usernameFromToken = authenticationController.jwtToken.getUsernameFromToken(
                wrap.getPasscode().startsWith("Bearer ") ? wrap.getPasscode().replaceAll("Bearer ", "").trim() :
                        wrap.getPasscode()).trim();
        try {
            User userByEmail = userController.getUserByEmail(usernameFromToken);
            if (user != null)
                user.setUser(userByEmail);
            else System.out.println("nulllll");
            System.out.println("EMAIL: " + userByEmail.getEmail());
        } catch (EmailNotExistsException e) {
            e.printStackTrace();
        } //todo: don't ignore this exception
        wrap.setUser(user);
        userController.addPrincipal(user);
    }


    @Override
    public void postSend(Message<?> message, MessageChannel channel, boolean sent) {
        StompHeaderAccessor wrap = StompHeaderAccessor.wrap(message);
        StompCommand command = wrap.getCommand();
        String destination = wrap.getDestination() != null ? wrap.getDestination() : "";
        String subscriptionId = wrap.getSubscriptionId() != null ? wrap.getSubscriptionId() : "";
        StompPrincipal user = (StompPrincipal) wrap.getUser();

        switch (command != null ? command : StompCommand.ABORT) {
            case CONNECT:
                handleToken(wrap, user);
            default:
        }
    }
}
