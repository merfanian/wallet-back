package wallet.back.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;
import wallet.back.configuration.CheckGroupAvailabilityInterceptor;
import wallet.back.configuration.StompPrincipal;
import wallet.back.domain.dto.CheckIdentifierDto;
import wallet.back.domain.dto.MessageDto;
import wallet.back.domain.entity.Gathering;
import wallet.back.domain.entity.User;
import wallet.back.exception.EmailNotExistsException;
import wallet.back.exception.TokenIsNotValidException;
import wallet.back.service.UserDetailsServiceImpl;
import wallet.back.service.UserService;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/wallet/api/user")
public class UserController {

    @Autowired
    UserDetailsServiceImpl detailsService;

    @Autowired
    UserService userService;

    Set<StompPrincipal> principals = new HashSet<>();

    @Autowired
    private SimpMessagingTemplate template;


    public void init() {
        CheckGroupAvailabilityInterceptor.userController = this;
    }

    @GetMapping("/activate/{token}")
    public ResponseEntity<MessageDto> activateAccount(@PathVariable String token) throws EmailNotExistsException, TokenIsNotValidException {
        User user = userService.activateUser(token);
        return ResponseEntity.ok(MessageDto.builder().message("done").build());
    }

    @GetMapping("/check_username/{username}")
    public ResponseEntity<CheckIdentifierDto> checkUsername(@PathVariable String username) {
        return ResponseEntity.ok(CheckIdentifierDto.builder().exists(userService.checkUsername(username)).build());
    }

    public User getUserByEmail(String email) throws EmailNotExistsException {
        return userService.findByEmail(email).orElseThrow(EmailNotExistsException::new);
    }

    public void addPrincipal(StompPrincipal principal) {
        principals.add(principal);
    }

    public void sendNewGatheringNotification(User user, Gathering gathering) {
        Optional<StompPrincipal> theUser = principals.stream().filter(principal -> principal.getUser().getId().equals(user.getId())).findAny();
        theUser.ifPresent(principal -> template.convertAndSendToUser(theUser.get().getName(), "/queue/notification", gathering));
    }
}
