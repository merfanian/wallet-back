package wallet.back.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import wallet.back.domain.dto.ReviewDto;
import wallet.back.domain.entity.Review;
import wallet.back.service.ReviewService;
import wallet.back.service.UserDetailsServiceImpl;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/wallet/api/review")
public class ReviewController {

    @Autowired
    ReviewService reviewService;

    @Autowired
    UserDetailsServiceImpl detailsService;


    @PostMapping()
    public ResponseEntity<ReviewDto> addReview(@RequestBody ReviewDto reviewDto) throws Exception {
        Review review = reviewService.addReview(reviewDto, detailsService.getUser().getProfile());
        return ResponseEntity.ok((ReviewDto) reviewDto.toDto(review));
    }

    @GetMapping
    public ResponseEntity<List<ReviewDto>> getReviews() {
        List<Review> reviews = reviewService.getReviews(detailsService.getUser().getProfile());
        List<ReviewDto> collect = reviews.stream().map(review -> (ReviewDto) new ReviewDto().toDto(review)).collect(Collectors.toList());
        return ResponseEntity.ok(collect);
    }

}
