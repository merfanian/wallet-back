package wallet.back.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import wallet.back.domain.dto.ImageDto;
import wallet.back.service.IStorageService;

import javax.servlet.http.HttpServletRequest;
import java.io.FileNotFoundException;
import java.io.IOException;

@RestController
@RequestMapping("/wallet/image")
public class ImageController {

    private final IStorageService storageService;

    @Autowired
    public ImageController(IStorageService storageService) {
        this.storageService = storageService;
    }


    @GetMapping("/download/avatar/{filename:.+}")
    public ResponseEntity<Resource> serveAvatar(@PathVariable String filename, HttpServletRequest request) throws FileNotFoundException {
        Resource resource = storageService.loadAvatarAsResource(filename);
        return serve(resource, request);
    }

    @GetMapping("/download/review/{filename:.+}")
    public ResponseEntity<Resource> serverReview(@PathVariable String filename, HttpServletRequest request) throws FileNotFoundException {
        Resource resource = storageService.loadReviewAsResource(filename);
        System.out.println("----- DOWNLOADING -----" + filename);
        return serve(resource, request);
    }

    @PostMapping(value = "/upload/avatar", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ImageDto storeAvatar(@RequestParam MultipartFile file) {
        System.out.println("----- UPLOADING -----");
        return ImageDto.builder().name(storageService.storeAvatar(file)).build();
    }


    private ResponseEntity<Resource> serve(Resource resource, HttpServletRequest request) {
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            throw new RuntimeException("error in controller");
        }
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

}
