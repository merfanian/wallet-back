package wallet.back.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import wallet.back.controller.UserController;
import wallet.back.domain.dto.ChatMessageDto;
import wallet.back.domain.dto.CreateGatheringDto;
import wallet.back.domain.dto.InviteNewUserDto;
import wallet.back.domain.dto.InviteUserToGatheringDto;
import wallet.back.domain.entity.Gathering;
import wallet.back.domain.entity.Message;
import wallet.back.domain.entity.Profile;
import wallet.back.domain.entity.User;
import wallet.back.exception.GatheringNotFoundException;
import wallet.back.repository.GatheringRepository;
import wallet.back.repository.MessageRepository;
import wallet.back.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class GatheringService {

    @Autowired
    GatheringRepository gatheringRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    MessageRepository messageRepository;

    @Autowired
    UserService userService;

    @Autowired
    UserController userController;

    public Gathering createGathering(CreateGatheringDto gatheringDto, Profile profile) {
        List<Profile> profiles = new ArrayList<>();
        profiles.add(profile);

        Gathering gathering = Gathering.builder()
                .avatar(gatheringDto.getAvatar())
                .title(gatheringDto.getTitle())
                .id(UUID.randomUUID())
                .profiles(profiles)
                .build();

        gatheringRepository.save(gathering);
        userController.sendNewGatheringNotification(profile.getUser(), gathering);
        return gathering;
    }

    public void inviteUserToGathering(InviteUserToGatheringDto gatheringDto, String gatheringId, Profile profile) throws GatheringNotFoundException {
        Optional<User> user = gatheringDto.isUsername() ? userRepository.findByUsername(gatheringDto.getUsername()) :
                userRepository.findByEmail(gatheringDto.getEmail());

        if (!user.isPresent()) {
            userService.inviteUser(InviteNewUserDto.builder().email(gatheringDto.getEmail())
                    .from(profile.getFirstName() + " " + profile.getLastName()).build());
            return;
        }

        Gathering gathering = profile.getGatherings().stream()
                .filter(gtr -> gtr.getId().toString().equals(gatheringId)).findFirst()
                .orElseThrow(GatheringNotFoundException::new);


        if (!user.get().getProfile().getGatherings().contains(gathering)) {
            user.get().getProfile().getGatherings().add(gathering);
            gathering.getProfiles().add(user.get().getProfile());
        }
        gatheringRepository.save(gathering);
        userRepository.save(user.get());

        userController.sendNewGatheringNotification(user.get(), gathering);
    }

    public List<Profile> getProfiles(String id) throws GatheringNotFoundException {
        return gatheringRepository.findById(UUID.fromString(id)).orElseThrow(GatheringNotFoundException::new).getProfiles();
    }

    public Message saveMessage(ChatMessageDto messageDto, Profile profile, String id) throws GatheringNotFoundException {
        Gathering gathering = gatheringRepository.findById(UUID.fromString(id)).orElseThrow(GatheringNotFoundException::new);
        Message build = messageDto.fromDto().toBuilder().profile(profile).gathering(gathering).build();
        gathering.getMessages().add(build);
        messageRepository.save(build);
        return build;
    }

    public List<Message> getGatheringMessages(String id) throws GatheringNotFoundException {
        return gatheringRepository.findById(UUID.fromString(id)).orElseThrow(GatheringNotFoundException::new).getMessages();
    }

    public Gathering updateGathering(CreateGatheringDto gatheringDto, Profile profile) throws GatheringNotFoundException {
        Gathering gathering = gatheringRepository.findById(gatheringDto.getId()).orElseThrow(GatheringNotFoundException::new);
        if (gathering.getProfiles().contains(profile))
            return gatheringRepository.save(gatheringDto.updateDto(gathering));
        return gathering;
    }
}
