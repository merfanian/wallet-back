package wallet.back.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import wallet.back.configuration.StorageConfig;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Date;

@Service
public class ImageStorageService implements IStorageService {

    protected final Path avatarPath;
    protected final Path reviewPath;

    @Autowired
    public ImageStorageService(StorageConfig storageConfig) {
        this.avatarPath = Paths.get(storageConfig.getAvatarsPath());
        this.reviewPath = Paths.get(storageConfig.getReviewPath());
    }

    @Override
    public void init() {
        try {
            Files.createDirectories(reviewPath);
            Files.createDirectories(avatarPath);
        } catch (IOException e) {
            throw new RuntimeException("could not initialize");
        }

    }

    @Override
    public String storeAvatar(MultipartFile file) {
        return store(file, avatarPath);
    }

    @Override
    public String storeReview(MultipartFile file) {
        return store(file, reviewPath);
    }

    @Override
    public Path loadAvatar(String fileName) {
        return load(fileName, avatarPath);
    }

    @Override
    public Path loadReview(String fileName) {
        return load(fileName, reviewPath);
    }

    @Override
    public Resource loadAvatarAsResource(String fileName) throws FileNotFoundException {
        return loadAsResource(fileName, avatarPath);
    }

    @Override
    public Resource loadReviewAsResource(String fileName) throws FileNotFoundException {
        return loadAsResource(fileName, reviewPath);
    }

    private String store(MultipartFile file, Path path) {
        String filename = StringUtils.cleanPath(file.getOriginalFilename());
        filename = new Date().getTime() + "."
                + file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1);
        try {
            if (file.isEmpty())
                throw new IllegalArgumentException("failed to store empty file" + filename);
            if (filename.contains(".."))
                throw new IllegalArgumentException("relative path error" + filename);
            try (InputStream inputStream = file.getInputStream()) {
                Files.copy(inputStream, path.resolve(filename), StandardCopyOption.REPLACE_EXISTING);
                return filename;
            }

        } catch (IOException e) {
            throw new RuntimeException("store failed ");
        }
    }

    private Path load(String filename, Path path) {
        return path.resolve(filename);
    }

    private Resource loadAsResource(String filename, Path path) throws FileNotFoundException {
        try {
            Path file = load(filename, path);
            return new UrlResource(file.toUri());
        } catch (MalformedURLException e) {
            throw new FileNotFoundException("can't find file");
        }
    }

}
